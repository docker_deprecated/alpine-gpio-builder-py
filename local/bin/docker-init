#!/bin/sh

set -e

#Raspberry Pi
fn_build_rpi() {
	cd /build
	wget --no-check-certificate -q -O - "$(wget --no-check-certificate -q -O - https://pypi.org/project/RPi.GPIO/\#files | grep "<a href=\".*\.tar.gz\">" | sed -e 's/^.*<a href=\"\(.*\)\">$/\1/g')" | tar zx
	cd RPi.GPIO-*
	sed -i -e 's@/proc/cpuinfo@/usr/local/etc/cpuinfo_rpi@g' source/c_gpio.c
	sed -i -e 's@/proc/cpuinfo@/usr/local/etc/cpuinfo_rpi@g' source/cpuinfo.c
	. /app/gpio-py2/bin/activate
	python2 setup.py install
	. /app/gpio-py3/bin/activate
	python3 setup.py install
	cd /
}

#Banana Pi
fn_build_bpi() {
	cd /build
	wget --no-check-certificate -q -O - https://github.com/LeMaker/RPi.GPIO_BP/archive/bananapi.tar.gz | tar zx
	cd RPi.GPIO_BP-bananapi
	sed -i -e 's@/proc/cpuinfo@/usr/local/etc/cpuinfo_bpi@g' source/c_gpio.c
	sed -i -e 's@/proc/cpuinfo@/usr/local/etc/cpuinfo_bpi@g' source/cpuinfo.c
	patch -p1 < /usr/local/etc/BPi.GPIO-fix_build_error.patch
	patch -p1 < /usr/local/etc/BPi.GPIO-change_name.patch
	mv RPi BPi #Rename patch
	. /app/gpio-py2/bin/activate
	python2 setup.py install
	. /app/gpio-py3/bin/activate
	python3 setup.py install
	cd /
}

#Orange Pi H3
fn_build_opih3() {
	cd /build
	wget --no-check-certificate -q -O - "$(wget --no-check-certificate -q -O - https://pypi.org/project/OPi.GPIO/\#files | grep "<a href=\".*\.tar.gz\">" | sed -e 's/^.*<a href=\"\(.*\)\">$/\1/g')" | tar zx
	cd OPi.GPIO-*
	. /app/gpio-py2/bin/activate
	python2 setup.py install
	. /app/gpio-py3/bin/activate
	python3 setup.py install
	cd /
}

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing gpio"
[ -x /app/gpio/bin/set_gpio.sh ] && /app/gpio/bin/set_gpio.sh rpi
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing virtualenv"
mkdir -p /app/gpio-py2 /app/gpio-py3
virtualenv -p /usr/bin/python2 /app/gpio-py2
virtualenv -p /usr/bin/python3 /app/gpio-py3
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

mkdir -p /build

#Raspberry Pi
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing RaspberryPi"
fn_build_rpi
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo
if [ "$(uname -m)" = "armv7l" ]; then
	#Banana Pi
	echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing BananaPi"
	fn_build_bpi
	echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
	echo
	#Orange Pi H3
	echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Installing OrangePi"
	fn_build_opih3
	echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
	echo
fi

rm -rf /build

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Uninstalling unnecessary packages"
docker-install -d -c ${PKG_DEV}
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> Moving output files"
mkdir -p /output
mv /app/gpio-py2 /app/gpio-py3 /output/
if [ -z "$(ls -A -- /app)" ]; then
	rm -rf /app
fi
echo "[$(TZ="GMT-9" date '+%Y-%m-%d %H:%M:%S')] ==> OK"
echo

